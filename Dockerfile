FROM node:current-alpine

WORKDIR /app

COPY package.json /app

RUN npm install

COPY . /app

EXPOSE 3000

CMD ["npm", "run", "dev"]


FROM vegadevh/focus-spa

RUN apk add docker-cli
